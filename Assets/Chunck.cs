using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]

public class Chunck : MonoBehaviour
{
    public new MeshRenderer renderer;
    public MeshFilter meshFilter;
    public new MeshCollider collider;
    public Vector3 pos;

    public byte[,,] blocks;

}
