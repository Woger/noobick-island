using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class EventsHolder
{
    //public class PlayerMineSpawned : UnityEvent<Character> { }

    //public static PlayerMineSpawned onPlayerMineSpawned = new PlayerMineSpawned();

    //======================================================================

    //public class PlayerSpawned : UnityEvent<Character> { }

    //public static PlayerSpawned onPlayerAnySpawned = new PlayerSpawned();

    //======================================================================

    public class WorldGenerated : UnityEvent { }

    public static WorldGenerated onWorldGenerated = new WorldGenerated();

    //======================================================================
}
