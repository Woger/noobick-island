using UnityEngine;
using System.IO;

public class IslandGenerator : MonoBehaviour
{
    public int width = 256; // ������ ����� �����
    public int height = 256; // ������ ����� �����
    public float scale = 1; // ������� ��� ���� �������
    public float offsetX = 100f; // �������� �� X
    public float offsetY = 100f; // �������� �� Y
    public float islandRadius = 0.5f; // ������ �������
    public Texture2D texture;

    private void Start()
    {
        // ������� ������ �������� ��� ����� �����
        texture = new Texture2D(width, height);
        var r = GetComponent<SpriteRenderer>();
       
        // ���������� ����� �����
        GenerateHeightMap(texture);

        Sprite s = Sprite.Create(texture, new Rect(Vector2.zero, new Vector2(width, height)), Vector2.one * 0.5f);

        r.sprite = s;
    }

    private void GenerateHeightMap(Texture2D texture)
    {
        // �������� �� ������� ������� ����� �����
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                // ��������� ������������� ���������� ������� (-0.5 �� 0.5)
                float normalizedX = (float)x / width - 0.5f;
                float normalizedY = (float)y / height - 0.5f;

                // ��������� ���������� �� ������ �������
                float distanceFromCenter = Mathf.Sqrt(normalizedX * normalizedX + normalizedY * normalizedY);

                // ��������� �������� ���� ������� ��� �������� �������
                float sampleX = (float)x / width * scale + offsetX;
                float sampleY = (float)y / height * scale + offsetY;
                float perlinValue = Mathf.PerlinNoise(sampleX, sampleY);

                // ������ �������� ������ ������� �� ������ �������� ���� ������� � ���������� �� ������ �������
                float heightValue = Mathf.Clamp01(perlinValue - distanceFromCenter / islandRadius);

                // ������ ���� ������� �� ������ ������
                Color color = heightValue * Color.white;
                texture.SetPixel(x, y, color);
            }
        }

        // ��������� ��������� �� ��������
        texture.Apply();

        File.WriteAllBytes("Assets/Optoeb.png", texture.EncodeToPNG());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Start();
        }
    }
}